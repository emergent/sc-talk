// SuperCollider Talk – Beispielkomposition
(
t = TempoClock(72/60).permanent_(true);

SynthDef(\saws, {
	arg out = 0, atk = 0.1, decay = 0.2, suslev = 1, rel = 2, crv = -2, gate = 1, freq = 440, width = 0.001, filtPhase = 1.5pi, filtMin = 2400, filtMax = 12000, amp = -3.dbamp, dt = 0.18, dtfreq = 0.25, pan = 0;
	var sig, env, dtune, filtersweep;

	env = Env.adsr(atk, decay, suslev, rel).kr(Done.freeSelf, gate);
	dtune = LFNoise2.kr(dtfreq!7).bipolar(dt).midiratio;
	filtersweep = SinOsc.kr(1/16, filtPhase).range(filtMin, filtMax);

	sig = VarSaw.ar(freq * dtune, 0, width, 0.1).sum;
	sig = LPF.ar(sig, filtersweep);

	sig = sig * env * amp;

	sig = Pan2.ar(sig, pan);

	Out.ar(out, sig);
}).add;

// Patterns
// "Sphärisches Brummen"

~basspad = Pbind(
	\instrument, \saws,
	\scale, Scale.minor,
	\octave, 3,
	\ctranspose, -3,
	\degree, Pseq([
		Pseq([0, -2], 4),
		0
	]),
	\filtMin, 880,
	\filtMax, 8192,
	\dur, 16,
	\legato, 0.97,
	\db, -4.5
);

// "plucked"
~pluck = Pbind(
	\instrument, \saws,
	\scale, Scale.minor,
	\ctranspose, -3,
	\octave, 6,
	\degree, Pseq([7, 4, 0, 5, 2, 0], 4),
	\amp, Pseq([-3, -6, -6, -4.5, -7, -7.5].dbamp, inf),
	\dur, 0.5,
	\atk, 0.001,
	\decay, 0.5,
	\crv, -8,
	\suslev, 0.1,
	\rel, 0.75,
	\pan, Pseq([
		Pseries(-1, 0.25, 8),
		Pseries(1, -0.25, 8)
	], inf),
	\legato, Pwhite(0.35, 0.5)
);

// "Lead-Melodie"
~mono = Pmono(
	\saws,
	\scale, Scale.minor,
	\ctranspose, -3,
	\octave, 5,
	\degree, Pseq([0, 4, 7, 2, 5], 4),
	\filtPhase, 0.5pi,
	\filtMin, 4800,
	\dur, 0.5,
	\amp, Pseq([-4.5, -6, -6, -5, -9].dbamp, inf),
	\atk, 0.5,
	\rel, 1.5,
	\suslev, 1,
	\width, 0.55,
	\legato, 1.05,
	\dt, 0.1,
	\pan, Pseq([
		Pseries(1, -0.5, 4),
		Pseries(-1, 0.5, 4)
	], inf),
);

~monomel = ~mono.play(t);

// Bassline
~bassline = Pbind(
	\instrument, \saws,
	\scale, Scale.minor,
	\octave, 4,
	\ctranspose, -3,
	\dur, 1,
	\degree, Pseq([
		Pseq([0, 4, 5, 2], 4),
		Pseq([-2, 2, 3, 0], 4)
	], 3),
	\filtPhase, 0.5pi,
	\filtMin, 6000,
	\legato, Plprand(0.5, 0.6),
	\atk, 0.05,
	\decay, 0.5,
	\suslev, 0.3,
	\rel, 0.6,
	\crv, -6,
	\width, Pwhite(0.25, 0.6),
	\amp, Pseq([0.9, 0.75, 0.75, 0.75]* 1.1, inf)
);

~piece = Ptpar([
	0, ~basspad,
	8, Pwrand(
		[~pluck, ~mono],
		[0.6, 0.4],
		4),
	32, ~bassline,
	64, Pwrand(
		[~pluck, ~mono],
		[0.4, 0.6],
		4),
]).play(t, quant:t.beatsPerBar);
)